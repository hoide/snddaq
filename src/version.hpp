#ifndef VERSION_HPP__
#define VERSION_HPP__
namespace {
	namespace version {
		static const std::string date="Wed Apr 14 11:52:24 JST 2021";
		static const std::string platform="Linux";
		static const std::string version="0.4";
	}; /* namespace version */
} /* anonymous namespace */
#endif /* VERSION_HPP__ */
