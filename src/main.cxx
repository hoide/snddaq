#include "CLI11.hpp"
#include "options.hpp"
#include "snddaq.hpp"

#include <sndfile.hh>

#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>

#include <TApplication.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TString.h>

#include <thread>


bool progress_callback(size_t percent)
{
    std::cerr << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bconverting: " << percent << "%";
    return true;
}

TH1F* h1 { nullptr };

void process( Options options ) {
    // open sound file
    //SndfileHandle wav(options.input_file_name.c_str());
    SndfileHandle wav( "./.fifo" );

    // Handle error
    if ( wav.error() )
    {
        std::cerr << "Error opening audio file '" << options.input_file_name << "'" << std::endl;
        std::cerr << "Error was: '" << wav.strError() << "'" << std::endl; 
        return;
    }

    compute_waveform( wav, 100, { Options::LEFT },
                     options.use_db_scale, options.db_min, options.db_max,
                     progress_callback, h1 );

}



int cli( Options options, TApplication* app ) {
    
    CLI::App snddaq {"snddaq"};

    // recording
    unsigned record_time { 0 };
    auto record_func = [&]() {
                           std::cout << "record_time = " << record_time << std::endl;
                           gSystem->Exec( Form("arecord --device plughw:1,0 --format S16_LE -d %u -r 88200 .fifo &", record_time ) );
                           process( options );
                       };

    auto record = snddaq.add_subcommand("record");
    record->add_option("time,-t,--time", record_time, "Duration of data taking on recording.")->default_val("10");
    record->parse_complete_callback( record_func );

    auto record_s = snddaq.add_subcommand("r");
    record_s->add_option("time,-t,--time", record_time, "Duration of data taking on recording.")->default_val("10");
    record_s->parse_complete_callback( record_func );

    // clear histogram
    auto clear_func = [&]() {
                          h1->Reset();
                          gPad->Modified();
                          gPad->Update();
                      };
    
    auto clear = snddaq.add_subcommand("clear");
    clear->parse_complete_callback( clear_func );

    auto clear_s = snddaq.add_subcommand("c");
    clear_s->parse_complete_callback( clear_func );
    
    // change histogram binning
    std::vector<double> binning;
    auto hist_func = [&]() {
                          delete h1;
                          h1 = new TH1F("spectrum", ";Pulse Height Estimate;", (int)(binning.at(0)), -0.05, 1.05 );
                          h1->SetLineColor(kBlack);
                          h1->SetFillColorAlpha(kCyan, 0.1);
                          h1->Draw();
                          gPad->Modified();
                          gPad->Update();
                      };
    
    auto hist = snddaq.add_subcommand("hist");
    hist->add_option("bins", binning, "Binning");
    hist->parse_complete_callback( hist_func );

    auto hist_s = snddaq.add_subcommand("h");
    hist_s->add_option("bins", binning, "Binning");
    hist_s->parse_complete_callback( hist_func );

    // histogram title
    std::string title_str;
    auto title = snddaq.add_subcommand("title");
    title->add_option("title", title_str, "Set histogram title");
    title->parse_complete_callback( [&](){ h1->SetTitle( title_str.c_str() ); gPad->Modified(); gPad->Update(); } );

    // save data
    std::string savefilename { "out" };
    
    auto save_func = [&]() {
                         if( savefilename.find( ".txt" ) != std::string::npos ) {
                         } else {
                             gPad->SaveAs( savefilename.c_str() );
                         }
                     };
    
    auto save = snddaq.add_subcommand("save");
    
    save->add_option("name", savefilename, "Save filename");
    
    save->parse_complete_callback( save_func );
    

    
    while( true ) {
        
        record_time = 10;
        
        std::string command;
        std::cout << "> " << std::flush;
        std::getline(std::cin, command);

        std::cout << command << std::endl;
        command = "snddaq " + command;
        
        std::vector<char*> args;
        std::istringstream iss( command );

        std::string token;
        while(iss >> token) {
            char *arg = new char[token.size() + 1];
            copy(token.begin(), token.end(), arg);
            arg[token.size()] = '\0';
            args.emplace_back(arg);
        }

        try {

            snddaq.parse( args.size(), &( args.at(0) ) );

            for(size_t i = 0; i < args.size(); i++) {
                delete[] args[i];
            }
            
        } catch(const CLI::ParseError &e) {
            
            if( e.get_exit_code() != static_cast<int>( CLI::ExitCodes::Success )) {
                std::cerr << e.what() << std::endl;
            }
            
            continue;

        } catch(...) {
            
            continue;
            
        }

    }

    return 1;
}



int main(int argc, char* argv[])
{
  Options options(argc, argv);

  auto* app = new TApplication("App", &argc, argv);

  gStyle->SetOptStat(1111111);

  new TCanvas("canvas", "Sound DAQ Campus", 0, 0, 1200, 800);

  h1  = new TH1F("spectrum", ";Pulse Height Estimate;", 1050, -0.05, 1.05 );
  h1->SetLineColor(kBlack);
  h1->SetFillColorAlpha(kCyan, 0.1);
  h1->Draw();
  gPad->Modified();
  gPad->Update();

  // command line thread
  std::thread t_cli( cli, options, app );

  app->Run();

  t_cli.join();

  delete app;

  return 0;
}
