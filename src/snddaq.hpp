#include <sndfile.hh>
#include "options.hpp"

class TH1F;

#ifdef __OBJC__
    typedef bool (^progress_callback_t)(size_t progress);
#else /* __OBJC__ */
    typedef bool (*progress_callback_t)(size_t progress);
#endif /* __OBJC__ */

void compute_waveform(
  const SndfileHandle& wav,
  size_t samples,
  Options::Channels channels,
  bool use_db_scale,
  float db_min,
  float db_max,
  progress_callback_t progress_callback,
  TH1F *hist
);


/*
  clamp x into range [min...max]
*/
template <typename T>
const T& clamp(const T& x, const T& min, const T& max)
{
  return std::max(min, std::min(max, x));
}

/*
  metaprogramming functions to get value range of sample format T.
*/
template <typename T> struct sample_scale {};

template <> struct sample_scale<short>
{
  static const unsigned short value = 1 << (sizeof(short)*8-1);
};

template <> struct sample_scale<float>
{
  static const int value = 1;
};




template <typename T>
T compute_sample(const std::vector<T>& block, int i, int n_channels, Options::Channel channel)
{
  switch(channel)
  {
    case Options::LEFT : return block[i];
    case Options::RIGHT: assert( n_channels == 2 ); return block[i+1];
    case Options::MID  : assert( n_channels == 2 ); return (block[i] + block[i+1]) / T(2);
    case Options::SIDE : assert( n_channels == 2 ); return (block[i] - block[i+1]) / T(2);
    case Options::MIN  : assert( n_channels == 2 ); return std::min(block[i], block[i+1]);
    case Options::MAX  : assert( n_channels == 2 ); return std::max(block[i], block[i+1]);
    default : assert(false && "no such channel"); break;
  }
  return T(0);
}

