#include "snddaq.hpp"

#include <cmath>
#include <iostream>
#include <vector>
#include <deque>
#include <assert.h>
#include <sndfile.hh>

#include <TMath.h>
#include <TGraph.h>
#include <TF1.h>
#include <TH1F.h>
#include <TRandom2.h>
#include <TPad.h>

/*
  conversion from and to dB
*/
float float2db(float x)
{
  x = fabsf(x);

  if (x > 0.0f)
    return 20.0f * log10f(x);
  else
    return -9999.9f;
}

float db2float(float x)
{
  return powf(10.0f, x/20.f);
}

/*
  map value x in range [in_min...in_max] into range [out_min...out_max]
*/
float map2range(float x, float in_min, float in_max, float out_min, float out_max)
{
  return clamp<float>(
    out_min + (out_max-out_min)*(x-in_min)/(in_max-in_min),
    out_min,
    out_max
  );
}


float genEmuPulse( bool recycle = false, double ampl_ref = 0.5, double noise = 5.e-3 ) {
    static double phase = gRandom->Uniform(0, 1);
    static double x = -5.0;
    static double amp = gRandom->Gaus( ampl_ref, 0.0001);
    static double sigma = 1.0;

    if( recycle ) {
        phase = gRandom->Uniform();
        x = -5.0;
        amp = gRandom->Gaus( ampl_ref, noise );
    }

    auto val = amp * TMath::Gaus( x, phase, sigma ) + gRandom->Gaus( 0.0, noise );
    x += 1.0;
    
    return val;
}


/* functor to filter out invalid channels
* lambdas would be nicer, but hey, it' 2015 :)
* */
struct filter_invalid_channels
{
  filter_invalid_channels(const SndfileHandle& wav)
  :wav(wav){}

  bool operator () (Options::Channel channel)
  {
    if ((channel == Options::MID  ||
         channel == Options::SIDE ||
         channel == Options::RIGHT||
         channel == Options::MIN  ||
         channel == Options::MAX) &&
         wav.channels() == 1
    )
    {
      std::cerr << "Warning: you're trying to generate output for channel '" << channel << "', but the input has only one channel. removing requested channel." << std::endl;
      return true;
    }
    return false;
  }

  private:
    const SndfileHandle& wav;
};


/*
  compute the waveform of the supplied audio-file and store it into out_image.
*/
void compute_waveform(
  const SndfileHandle& wav,
  size_t samples,
  Options::Channels channels,
  bool use_db_scale,
  float db_min,
  float db_max,
  progress_callback_t progress_callback,
  TH1F* h1
)
{
  using std::size_t;
  using std::cerr;
  using std::endl;

  // you can change it to float or short, short was much faster for me.
#ifdef USE_FLOAT
  typedef float sample_type;
#else
  typedef short sample_type;
#endif

  samples = (size_t) std::min(wav.frames(), (sf_count_t) samples);

  int frames_per_pixel  = std::max<int>(1, static_cast<const int &>(wav.frames() / (samples == 0 ? 1 : samples)));
  int samples_per_pixel = wav.channels() * frames_per_pixel;
  std::size_t progress_divisor = std::max<std::size_t>(1, samples/100);

  std::cout << "frames_per_pixel = " << frames_per_pixel << std::endl;
  std::cout << "samples_per_pixel = " << samples_per_pixel << std::endl;

  // temp buffer for samples from audio file
  std::vector<sample_type> block(samples_per_pixel);

  // filter out channels, that require more channels than the wav file has
  filter_invalid_channels filter( wav );
  channels.erase(
      std::remove_if(channels.begin(), channels.end(), filter),
      channels.end()
  );

  if (channels.empty())
  {
    std::cerr << "Warning: there are no channels left to process, aborting." << endl;
    return;
  }
  
  std::cout << "channels = " << channels.size() << std::endl;

  // create one vector of floats for each requested channel
  std::vector<std::vector<float> > output_values( channels.size() );

  // https://github.com/beschulz/wav2json/pull/7
  // http://www.mega-nerd.com/libsndfile/api.html#note2
  const_cast<SndfileHandle&>(wav).command(SFC_SET_SCALE_FLOAT_INT_READ, 0, SF_TRUE);

  std::deque< float > waveform;
  
  /*
    the processing works like this:
    for each vertical pixel in the image (x), read frames_per_pixel frames from
    the audio file and find the min and max values.
  */

  auto* fit = new TF1("fit", "gaus", 10, 30 );
  auto* gr  = new TGraph;
  gr->SetName("event");
  gr->SetMarkerStyle(20);

  
  for (size_t x = 0; x < samples; ++x)
  {
    // read frames
    sf_count_t n = const_cast<SndfileHandle&>(wav).readf(&block[0], frames_per_pixel) * wav.channels();
    assert(n <= (sf_count_t)block.size());
    
    // for each requested channel
    for(size_t channel_idx = 0; channel_idx != channels.size(); ++channel_idx)
    {

      Options::Channel channel = channels[channel_idx];
      for (int i=0; i<n; i+=wav.channels()) // seek to next frame
      {
        sample_type sample = compute_sample(block, i, wav.channels(), channel);
        
        float y = use_db_scale?
            map2range( float2db(sample / (float)sample_scale<sample_type>::value ), db_min, db_max, 0, 1):
            map2range( sample, 0, sample_scale<sample_type>::value, 0, 1);

        y += genEmuPulse( gRandom->Uniform() < 1.e-2, 0.5, 1.e-3 );
        
        waveform.emplace_back( y );
        //sample_type abs_sample = std::abs(sample);
        //max = std::max( max, abs_sample );
      }

      // gating
      {
          bool isGated { false };
          size_t counter { 0 };
          size_t lastGated { 0 };

          std::vector<float> packet;
          
          for( size_t j=0; j<waveform.size(); j++ ) {
              
              if( !isGated && waveform.at(j) > 0.1 ) {
                  isGated = true;
                  counter = 0;
                  packet.clear();

                  // back-fill
                  for( size_t k = (j>=20)? j-20 : 0; k<j; k++ ) {
                      packet.emplace_back( waveform.at(k) );
                  }
              }

              if( isGated && counter > 30 ) {
                  isGated = false;
                  counter = 0;
                  //events.emplace_back( packet );

                  gr->Set(0);
                  for( auto& y : packet ) {
                      gr->SetPoint( gr->GetN(), gr->GetN(), y );
                  }
      
                  fit->SetParameter(0, 0.5);
                  fit->SetParameter(1, 20);
                  fit->FixParameter(2, 1);
                  gr->Fit( fit->GetName(), "rq0" );
                  h1->Fill( fit->GetParameter(0) );

                  lastGated = j;
              }
              
              if( isGated ) {
                  packet.emplace_back( waveform.at(j) );
                  counter++;
              }
              
          }
          
          if( lastGated > 0 ) {
              waveform.erase( waveform.begin(), waveform.begin()+lastGated );
          } else {
              waveform.clear();
          }
          
      }

      h1->Draw();
      gPad->Modified();
      gPad->Update();
      
      // print progress
      if ( x%(progress_divisor) == 0 )
      {
        if ( progress_callback && !progress_callback( 100*x/samples ) )
          return;
      }

    }
  }

  delete gr;
  delete fit;

  // call the progress callback
  if ( progress_callback && !progress_callback( 100 ) ) {
      std::cout << std::endl;
      return;
  }
}
