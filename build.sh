g++ -o snddaq -O3 -Wall src/*.cxx\
    -lboost_program_options -lsndfile  \
     `root-config --cflags` \
     -L/usr/local/root/root-latest/lib -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -rdynamic
